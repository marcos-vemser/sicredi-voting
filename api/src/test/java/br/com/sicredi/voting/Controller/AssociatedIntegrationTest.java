package br.com.sicredi.voting.Controller;

import br.com.sicredi.voting.Entity.AssociatedEntity;
import br.com.sicredi.voting.Service.AssociatedService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(AssociatedController.class)
public class AssociatedIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AssociatedService service;

    @Test
    public void createAssociated() throws Exception {
        AssociatedEntity associated = new AssociatedEntity();
        associated.setCpf(1111111111L);
        associated.setName("Associated Sicredi");
        given(service.saveAssociated(Mockito.any())).willReturn(associated);

        mvc.perform(post("/api/associated/new")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(associated)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Associated Sicredi")));


        Mockito.verify(service, VerificationModeFactory.times(1))
                .saveAssociated(Mockito.any());
        reset(service);
    }
}
