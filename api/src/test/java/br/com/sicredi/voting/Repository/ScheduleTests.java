package br.com.sicredi.voting.Repository;

import br.com.sicredi.voting.Entity.ScheduleEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ScheduleTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ScheduleRepository repository;

    private final String DESCRIPTION = "Testing the voting of Sicredi";

    @Before
    public void setUp() {
        ScheduleEntity schedule = new ScheduleEntity();
        schedule.setDescription(DESCRIPTION);
        repository.save(schedule);
    }

    @Test
    public void addSchedule(){
        ScheduleEntity scheduleFound = repository.findByDescription(DESCRIPTION);
        assertThat(scheduleFound.getDescription()).isNotNull();
    }

    @Test
    public void openSession(){
        ScheduleEntity schedule = repository.findByDescription(DESCRIPTION);
        schedule.setOpen(true);
        schedule.setDuration(4);
        repository.save(schedule);

        ScheduleEntity scheduleFound = repository.findByDescription(DESCRIPTION);

        assertThat(scheduleFound).isEqualTo(schedule);
        assertThat(scheduleFound.getOpen()).isTrue();
    }
}
