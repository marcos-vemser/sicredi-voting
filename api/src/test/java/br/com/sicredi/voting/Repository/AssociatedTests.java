package br.com.sicredi.voting.Repository;

import br.com.sicredi.voting.Entity.AssociatedEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AssociatedTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AssociatedRepository repository;

    @Test
    public void addAssociated(){
        AssociatedEntity associated = new AssociatedEntity();
        associated.setCpf(11111111111L);
        associated.setName("Test");

        entityManager.persist(associated);
        entityManager.flush();

        AssociatedEntity associatedFound = repository.findById(associated.getCpf()).get();

        assertThat(associatedFound).isEqualTo(associated);
    }
}
