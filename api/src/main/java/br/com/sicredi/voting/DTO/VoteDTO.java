package br.com.sicredi.voting.DTO;

public class VoteDTO {

    private Integer idSchedule;
    private Long cpfAssociated;
    private Boolean vote;

    public Integer getIdSchedule() {
        return idSchedule;
    }

    public void setIdSchedule(Integer idSchedule) {
        this.idSchedule = idSchedule;
    }

    public Long getCpfAssociated() {
        return cpfAssociated;
    }

    public void setCpfAssociated(Long cpfAssociated) {
        this.cpfAssociated = cpfAssociated;
    }

    public Boolean getVote() {
        return vote;
    }

    public void setVote(Boolean vote) {
        this.vote = vote;
    }
}
