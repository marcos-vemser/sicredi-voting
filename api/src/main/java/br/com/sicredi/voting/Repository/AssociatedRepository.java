package br.com.sicredi.voting.Repository;

import br.com.sicredi.voting.Entity.AssociatedEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociatedRepository extends CrudRepository<AssociatedEntity, Long> {
}
