package br.com.sicredi.voting.Repository;

import br.com.sicredi.voting.Entity.VotesEntity;
import br.com.sicredi.voting.Entity.VotesEntityId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VotesRepository extends CrudRepository<VotesEntity, VotesEntityId> {

    List<VotesEntity> findAllById_IdSchedule(Integer id);

}
