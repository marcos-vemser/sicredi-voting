package br.com.sicredi.voting.Repository;

import br.com.sicredi.voting.Entity.ScheduleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends CrudRepository<ScheduleEntity, Integer> {

    ScheduleEntity findByDescription( String description );

}
