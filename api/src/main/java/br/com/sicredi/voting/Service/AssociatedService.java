package br.com.sicredi.voting.Service;

import br.com.sicredi.voting.Entity.AssociatedEntity;
import br.com.sicredi.voting.Repository.AssociatedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AssociatedService {

    @Autowired
    private AssociatedRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public AssociatedEntity saveAssociated( AssociatedEntity associated ){
        return repository.save(associated);
    }

}
