package br.com.sicredi.voting.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class AssociatedEntity {

    @Id
    private Long cpf;
    private String name;

    @OneToMany( mappedBy = "associated", cascade = CascadeType.ALL)
    private List<VotesEntity> votes = new ArrayList<>();

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<VotesEntity> getVotes() {
        return votes;
    }

    public void setVotes(List<VotesEntity> votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object obj) {
        AssociatedEntity otherAssociated = (AssociatedEntity) obj;
        return this.getCpf() == otherAssociated.getCpf() && this.getName().equals(otherAssociated.getName());
    }
}
