package br.com.sicredi.voting.DTO;

public class SessionDTO {

    private Integer id;
    private Integer duration = 1;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
