package br.com.sicredi.voting.Service;

import br.com.sicredi.voting.DTO.ScheduleDTO;
import br.com.sicredi.voting.DTO.SessionDTO;
import br.com.sicredi.voting.Entity.ScheduleEntity;
import br.com.sicredi.voting.Repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.RollbackException;
import java.util.List;
import java.util.Optional;

@Service
public class ScheduleService {

    @Autowired
    private ScheduleRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ScheduleEntity saveSchedule( ScheduleDTO schedule ){
        ScheduleEntity entity = new ScheduleEntity(schedule.getDescription());
        return repository.save(entity);
    }

    @Transactional( rollbackFor = Exception.class )
    public ScheduleEntity openSessionSchedule( SessionDTO session ) {
        ScheduleEntity entity = repository.findById(session.getId()).get();
        entity.setDuration(session.getDuration());
        entity.setOpen(true);

        closedSchedule(entity, session.getDuration());

        return repository.save(entity);
    }

    public static void closedSchedule(ScheduleEntity entity, int time){
        new Thread(() -> {
            try {
                Thread.sleep(time*1000);
                entity.setOpen(false);
            }
            catch (Exception e){
                System.err.println(e);
            }
        }).start();
    }

}
