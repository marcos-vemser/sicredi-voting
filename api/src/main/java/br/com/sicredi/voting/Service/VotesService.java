package br.com.sicredi.voting.Service;

import br.com.sicredi.voting.DTO.VoteDTO;
import br.com.sicredi.voting.Entity.ScheduleEntity;
import br.com.sicredi.voting.Entity.VotesEntity;
import br.com.sicredi.voting.Entity.VotesEntityId;
import br.com.sicredi.voting.Repository.ScheduleRepository;
import br.com.sicredi.voting.Repository.VotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VotesService {

    @Autowired
    private VotesRepository repository;

    @Autowired
    private ScheduleRepository repositorySchedule;

    @Transactional( rollbackFor = Exception.class )
    public VotesEntity saveVotes( VoteDTO vote ){
        ScheduleEntity schedule = repositorySchedule.findById(vote.getIdSchedule()).get();

        if( schedule.getOpen() ){
            VotesEntityId id = new VotesEntityId(vote.getIdSchedule(), vote.getCpfAssociated());
            VotesEntity entity = new VotesEntity(id, vote.getVote());
            return repository.save(entity);
        }else{
            return null;
        }
    }

    public Integer countVotesForId( Integer id ){
        return repository.findAllById_IdSchedule(id).size();
    }

}
