package br.com.sicredi.voting.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class VotesEntityId implements Serializable {

    @Column(name = "ID_SCHEDULE")
    private Integer idSchedule;

    @Column(name = "CPF_ASSOCIATED")
    private Long cpfAssociated;

    public VotesEntityId() {}

    public VotesEntityId(Integer idSchedule, Long cpfAssociated) {
        this.idSchedule = idSchedule;
        this.cpfAssociated = cpfAssociated;
    }

}
