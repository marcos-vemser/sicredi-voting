package br.com.sicredi.voting.Entity;

import javax.persistence.*;

@Entity
public class VotesEntity {

    @EmbeddedId
    private VotesEntityId id;

    @ManyToOne
    @MapsId("ID_SCHEDULE")
    @JoinColumn( name = "ID_SCHEDULE", nullable = false)
    private ScheduleEntity schedule;

    @ManyToOne
    @MapsId("CPF_ASSOCIATED")
    @JoinColumn( name = "CPF_ASSOCIATED", nullable = false)
    private AssociatedEntity associated;

    private Boolean vote;

    public VotesEntity() {
    }

    public VotesEntity(VotesEntityId id, Boolean vote) {
        this.id = id;
        this.vote = vote;
    }

    public VotesEntityId getId() {
        return id;
    }

    public void setId(VotesEntityId id) {
        this.id = id;
    }

    public ScheduleEntity getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleEntity schedule) {
        this.schedule = schedule;
    }

    public AssociatedEntity getAssociated() {
        return associated;
    }

    public void setAssociated(AssociatedEntity associated) {
        this.associated = associated;
    }

    public Boolean getVote() {
        return vote;
    }

    public void setVote(Boolean vote) {
        this.vote = vote;
    }
}
