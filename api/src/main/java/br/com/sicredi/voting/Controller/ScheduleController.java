package br.com.sicredi.voting.Controller;

import br.com.sicredi.voting.DTO.ScheduleDTO;
import br.com.sicredi.voting.DTO.SessionDTO;
import br.com.sicredi.voting.Entity.ScheduleEntity;
import br.com.sicredi.voting.Service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( "/api/schedule" )
public class ScheduleController {

    @Autowired
    ScheduleService service;

    @PostMapping( value = "/new" )
    @ResponseBody
    public ScheduleEntity saveSchedule( @RequestBody ScheduleDTO schedule ){
        return service.saveSchedule(schedule);
    }

    @PostMapping( value = "/session-open" )
    @ResponseBody
    public ScheduleEntity OpenSessionSchedule( @RequestBody SessionDTO session ){
        return service.openSessionSchedule(session);
    }

}
