package br.com.sicredi.voting.Controller;

import br.com.sicredi.voting.Entity.AssociatedEntity;
import br.com.sicredi.voting.Service.AssociatedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( "/api/associated" )
public class AssociatedController {

    @Autowired
    AssociatedService service;

    @PostMapping( value = "/new" )
    @ResponseBody
    public AssociatedEntity saveAssociated( @RequestBody AssociatedEntity associated ){
        return service.saveAssociated(associated);
    }

}
