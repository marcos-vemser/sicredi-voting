package br.com.sicredi.voting.DTO;


public class ScheduleDTO {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
