package br.com.sicredi.voting.Controller;

import br.com.sicredi.voting.DTO.VoteDTO;
import br.com.sicredi.voting.Entity.AssociatedEntity;
import br.com.sicredi.voting.Entity.VotesEntity;
import br.com.sicredi.voting.Service.VotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( "/api/votes" )
public class VotesController {

    @Autowired
    VotesService service;

    @GetMapping( value = "/total/{id}" )
    @ResponseBody
    public Integer totalVotes( @PathVariable Integer id ){
        return service.countVotesForId(id);
    }

    @PostMapping( value = "/new" )
    @ResponseBody
    public VotesEntity saveVote(@RequestBody VoteDTO vote ){
        return service.saveVotes(vote);
    }

}
