package br.com.sicredi.voting.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ScheduleEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Integer id;
    private String description;
    private Integer duration;
    private Boolean isOpen = false;

    @OneToMany( mappedBy = "schedule", cascade = CascadeType.ALL)
    private List<VotesEntity> votes = new ArrayList<>();

    public ScheduleEntity() {}

    public ScheduleEntity(String description) {
        this.description = description;
    }

    public ScheduleEntity(Integer id, Integer duration, Boolean isOpen) {
        this.id = id;
        this.duration = duration;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<VotesEntity> getVotes() {
        return votes;
    }

    public void setVotes(List<VotesEntity> votes) {
        this.votes = votes;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }
}
