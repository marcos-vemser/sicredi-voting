# Sicredi Project for Voting - Test

### Used Stacks
    - Java 8
    - Springboot
    - Spring Data
    - JUnit
    - Mysql
    - Docker
        - Dockerfile
        - Docker Compose

### To Test

    - In Dev
        - Open in IDE
        - Set properties database
        - Start

    - In Homolog
        - Docker-compose up -d in folder root